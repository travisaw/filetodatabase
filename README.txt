
 ************************************************************************************************
 *
 * FILE TO DATABASE
 *
 * BY: TRAVIS WILSON (travis@totaltravisty.com)
 * v1.0 6/27/2013
 *
 * Contents:
 *  1) INTRODUCTION
 *  2) FILE DETAILS
 *  3) USAGE
 *
 ************************************************************************************************

 1) INTRODUCTION

    File to DB is a PHP script that reads the confiugration of a flat, fixed width, text file and
	reads its contents into a specified database.

 2) FILE DETAILS

    DataScripts.sql
        Run this script on desired database to setup configuration table

	File2Db.php
        Run this script from the command line to execute data load

 3) USAGE

    Populate fileToDb table with the details of the file to import.
        fileGroup: Unique logical name (6 char max) of file to import.
        destTable: SQL table name to import data to.
        sourceFile: Path of the source file to import
        clearTableFirst: Truncate (clear) table first? Set 0 for no and 1 for yes.
        startLine: Line to start import - useful on files where there is header data.

    Populate fileToDbDetail with data of each column to import.
        fileGroup: Unique file group name assigned in fileToDb.
        tableColumn: Name of the column in the SQL table to populate.
        posStart: The start posision (column number) to start reading in the source file.
        posLength: The number of columns to read in the source file.

    Configure File2Db.php
        Open File2Db.php in your favorite text editor.
        In the "variables" section is where you configure behavior
            1) Provide the database connection string variables.
                    Database Server Name/IP, Database Name, User Id, Password
            2) Test Only mode
                    Set this variable to 1 if you want the import to stop after one insert.
                    This is helpful when configuring a new import and you want to make sure
                    everything lines up okay.

            3) $fileInfoTable and $fileDetailTable
                    You shouldn't need to change these unless you change the table names used
                    in the configuration above

    Run File2Db.php from the command line.  Provide file group as input parameter:
        php File2Db.php TEST

        You can output results to a text file as well:
        php File2Db.php TEST >> test.txt

 ************************************************************************************************
 *
 * CSV TO DATABASE
 *
 * BY: TRAVIS WILSON (travis@totaltravisty.com)
 * v1.0 2018‑02‑05
 *
 * Contents:
 *  1) INTRODUCTION
 *  2) FILE DETAILS
 *  3) USAGE
 *
 ************************************************************************************************

 1) INTRODUCTION

    CSV To Database is a simple script to load raw data from CSV into an database table.

2) FILE DETAILS

    csvToDatabase.php
        This is the self contained script that will import the data.

3) USAGE

    Open csvToDatabase.php and edit variables

    $sourceFile = CSV File to load
    $delimiter = '|' = Line Delimiter
        (For end of line best to use addCharToLineEnd.php and add delimiter)
    $tableName = Destination Table Name

    DATABASE VARIABLES

    $CONN_DB_TYPE = 'mysql';
    $CONN_DB_HOST = Ex. 'localhost'
    $CONN_DB_NAME = Database Name
    $CONN_DB_USER = Database User
    $CONN_DB_PASS = Database Password

    This script will parse through a file and put all values into fields of the table specified. You have to create the desired output table manually and define the columns. Each column should be varchar and be named 'FIELD_N' where N is the field number.

