<?php


// FILE NAME AND DELIMITER
$inFileName = "";
$delimiter = ",";
$outFileName = "";
$fieldLength = array();

// OPEN FILE
$inHandle = fopen($inFileName, "r") or die("Unable to open input file!");

// LOOP THROUGH EACH ROW OF THE SOURCE FILE AND COUNT FIELD LENGTHS
$counter = 1;

while(!feof($inHandle)) {
    $sourceRow = fgets($inHandle);
    $parsedRow = str_getcsv($sourceRow, $delimiter);
    
    for ($r = 0; $r < count($parsedRow); $r++) {
        if ($counter == 1) {
            array_push($fieldLength, strlen($parsedRow[$r]));
        }
        else {
            if (strlen($parsedRow[$r]) > $fieldLength[$r]) { $fieldLength[$r] = strlen($parsedRow[$r]); }
        }
    }
    $counter++;
}

// SET CUMULATIVE TOTAL FOR FIELD LENGTHS
$fieldLengthSum = 0;
$fieldLengthCum = array();
for ($r = 0; $r < count($fieldLength); $r++) {
    $fieldLengthSum = $fieldLengthSum + $fieldLength[$r];
    array_push($fieldLengthCum, $fieldLengthSum);
}

// RESET FILE HANDLER TO BEGINNING OF FILE AND OPEN OUTPUT FILE FOR WRITING
$outHandle = fopen($outFileName, "w+") or die("Unable to open output file!");
rewind($inHandle);

// PROCESS AND WRITE OUTPUT

while(!feof($inHandle)) {

    $sourceRow = fgets($inHandle);
    $parsedRow = str_getcsv($sourceRow, $delimiter);
    $outString = "";

    for ($r = 0; $r < count($parsedRow); $r++) {
        $outString .= $parsedRow[$r];
        $outString = str_pad($outString, $fieldLengthCum[$r]);
    }
    
    $outString .= "\n";
    fwrite($outHandle, $outString);
}


// CLOSE SOURCE FILE
fclose($inHandle);
fclose($outHandle);

// SUMMARIZE
echo PHP_EOL;
for ($r = 0; $r < count($fieldLength); $r++) {
    echo "Column " . ($r + 1) . " is $fieldLength[$r] long ending at position $fieldLengthCum[$r]" . PHP_EOL;
}

?>

