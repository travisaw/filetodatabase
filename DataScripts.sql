/* ------------------------------------------------------------------------- */
/* TABLE: fileToDb */
CREATE TABLE `fileToDb` (
	`fileGroup` VARCHAR(6) NOT NULL,
	`destTable` VARCHAR(50) NOT NULL,
	`sourceFile` VARCHAR(100) NOT NULL,
	`clearTableFirst` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
	`startLine` INT(10) UNSIGNED NOT NULL DEFAULT '1',
	PRIMARY KEY (`fileGroup`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;

/* ------------------------------------------------------------------------- */
/* TABLE: fileToDbDetail */
CREATE TABLE `fileToDbDetail` (
	`fileGroup` VARCHAR(6) NOT NULL,
	`tableColumn` VARCHAR(50) NOT NULL,
	`posStart` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
	`posLength` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`fileGroup`, `tableColumn`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;
