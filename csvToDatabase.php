<?php

//------------------------------------------------------------------------------------------------------------------
// VARIABLES
$sourceFile = 'data.dat';
$delimiter = '|';

$tableName = 'data';

//------------------------------------------------------------------------------------------------------------------
// DATABASE CONNECTION

// Database Parameters
$CONN_DB_TYPE = 'mysql';
$CONN_DB_HOST = 'localhost';
$CONN_DB_NAME = 'test';
$CONN_DB_USER = '<User Name>';
$CONN_DB_PASS = '<User Password>';
$CONN_OPTIONS = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);

// Create connection
$con = new PDO($CONN_DB_TYPE . ':host=' . $CONN_DB_HOST . ';dbname=' . $CONN_DB_NAME, $CONN_DB_USER, $CONN_DB_PASS, $CONN_OPTIONS);
$con->exec("set names utf8");


//------------------------------------------------------------------------------------------------------------------
// READ DATA

$fileArray = array();
$fieldCount = 0;

$file = fopen($sourceFile, 'r');

while (($line = fgetcsv($file, 0, $delimiter)) !== FALSE) {
    //$line is an array of the csv elements
    array_push($fileArray, $line);

    if (count($line) > $fieldCount) {
        $fieldCount = count($line);
    }
    //print_r($line);
    //break;
}

fclose($file);

echo "Field Count is: $fieldCount \n";

//------------------------------------------------------------------------------------------------------------------
// CREATE TABLE

$fieldLoopCount = 1;
$sql = "CREATE TABLE $tableName (";

while ($fieldLoopCount <= $fieldCount) {

    $sql .= "FIELD_$fieldLoopCount VARCHAR(100)";
    if ($fieldLoopCount !== $fieldCount) {
        $sql .= ",";
    }
    $fieldLoopCount++;
}

$sql .= ");";

//$con->exec($sql);

//------------------------------------------------------------------------------------------------------------------
// INSERT DATA TO TABLE

$recordSuccessCount = 0;
$sqlInsert = "INSERT INTO $tableName (";
$fieldLoopCount = 1;

while ($fieldLoopCount <= $fieldCount) {

    $sqlInsert .= "FIELD_$fieldLoopCount";
    if ($fieldLoopCount !== $fieldCount) {
        $sqlInsert .= ", ";
    }
    $fieldLoopCount++;
}

$sqlInsert .= ") VALUES (";

//echo $sqlInsert . PHP_EOL;
//print_r($fileArray);

$con->exec("BEGIN");
foreach ($fileArray as $lineArray) {
    $sql = "";
    //print_r($lineArray);
    for ($x = 0; $x < count($lineArray); $x++) {
        //echo $lineArray[$x] . PHP_EOL;
        $lineArray[$x] = str_replace("'", "''", $lineArray[$x]);
        $sql .= "'$lineArray[$x]'";
        if ($x !== count($lineArray) - 1){
            $sql .= ", ";
        }
    }

    $sqlFinal = $sqlInsert . $sql . ");";
    // echo $sqlFinal . PHP_EOL;

    $con->exec($sqlFinal);
    $recordSuccessCount++;

}
$con->exec("COMMIT");

echo "Processed $recordSuccessCount records" . PHP_EOL;

?>