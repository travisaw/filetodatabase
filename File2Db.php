<?php

/*********************************************************************************************************/
// Check if file group provided in command line - exit if not.
if (isset($argv[1]) == true) {
	$fileGroup = trim($argv[1]); }
else {
	die ("No Filegroup specified - exiting\n"); }	

/*********************************************************************************************************/
// DATABASE VARIABLE
$DBSERVER = "localhost";
$DBNAME = "<DBNAME>";
$DBUSER = "<DBUSER>";
$DBPASS = "<DBPASS>";

// RUN MODE VARIABLE
$testOnly = 0; // SET TO 0 IF YOU'RE USING IN PRODUCTION, SET TO 1 IF YOU WANT TO DIE AFTER ONE INSERT STATEMENT

// CONSTANT VARIABLES
$fileInfoTable = "fileToDb";
$fileDetailTable = "fileToDbDetail";

$startTime = time();

/*********************************************************************************************************/
// Initialize database connection
$con;
$con = new mysqli($DBSERVER, $DBUSER, $DBPASS, $DBNAME);
if ($con->connect_errno) {
	die ("Failed to connect to MySQL: (" . $con->connect_errno . ") " . $con->connect_error);
}

/*********************************************************************************************************/
// Check $fileInfoTable exists

$sql = "SHOW TABLES LIKE '$fileInfoTable'";

if (!$result = $con->query($sql)) {
	die ("CALL failed: (" . $con->errno . ") " . $con->error) . "\n"; }

if ($result->num_rows < 1) {
	die ("File definition table $fileInfoTable doesn't exist - check configuration\n"); }

/*********************************************************************************************************/
// Get File and Table Info

$sql = "SELECT destTable, sourceFile, clearTableFirst, startLine FROM fileToDb WHERE fileGroup = '$fileGroup';";

if (!$result = $con->query($sql)) {
	die ("CALL failed: (" . $con->errno . ") " . $con->error) . "\n"; }

if ($result->num_rows > 0) {
	$row = $result->fetch_row();
	$destTable = $row[0];
	$sourceFile = $row[1];
	$clearTable = $row[2];
	$startLine = $row[3];
	}
else { die ("No definition found for filegroup $fileGroup - check configuration in $fileInfoTable\n"); }

/*********************************************************************************************************/
// Get Table Details

$sql = "SELECT tableColumn, posStart, PosLength FROM fileToDbDetail WHERE fileGroup = '$fileGroup' ORDER BY posStart;";

if (!$result = $con->query($sql)) {
	die ("CALL failed: (" . $con->errno . ") " . $con->error) . "\n"; }

if ($result->num_rows > 0) {
	$colCount = 0;
	while ($row = $result->fetch_row()) {
		$destColumn[$colCount] = $row[0];
		$colStart[$colCount] = $row[1];
		$colLength[$colCount] = $row[2];
		$colCount++;
		}
	}
else { die ("No column definitions found for filegroup $fileGroup - check configuration in $fileDetailTable\n"); }


/*********************************************************************************************************/
// Clear Dest Table if Flag Set

if ($clearTable == 1)
{
	$sql = "TRUNCATE TABLE $destTable;";

	if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error) . "\n"; }
}


/*********************************************************************************************************/
// Loop through file

// Open file for reading
$file = fopen($sourceFile, "r") or exit ("Unable to open file!");

// Check if file is empty, otherwise process
if (feof($file)) {
	die ("No Content in Source File $sourceFile");}


$lineCounter = 1;
$colLoop = 0;

// Build SQL statement
$sqlBase = "INSERT INTO $destTable (";
while ($colLoop < $colCount)
{
	if ($colLoop > 0) {$sqlBase .= ", ";}
	$sqlBase .= "`$destColumn[$colLoop]`";
	$colLoop++;
}
$sqlBase .= ") VALUES (";

/* Read Through File */
while(!feof($file))
{
	$colLoop = 0;
	$sql = $sqlBase;
	$line = fgets($file);
	
	/* If current line in the file is larger than specifed start line process the line */
	if($lineCounter >= $startLine) {
		while ($colLoop < $colCount)
		{
			if ($colLoop > 0) {$sql .= ", ";}
			$sql .= "'" . $con->real_escape_string(substr($line, $colStart[$colLoop] - 1, $colLength[$colLoop])) . "'";
			$colLoop++;
		}

		$sql .= ");";

		/* Execute SQL Statement */
		if (!$con->query($sql)) {
			die ("QUERY failed ROW $lineCounter: (" . $con->errno . ") " . $con->error) . "\n"; }

		/* IF TESTONLY MODE IS ENABLE TERMINATE */
		if ($testOnly == 1) {
			$con->close();
			fclose($file);
			echo $sql . "\n";
			die ("Test mode is enabled, Exiting!\n");
		}
		
	}
	
	$lineCounter++;
	
}


/*********************************************************************************************************/
// CLEANUP
$con->close();
fclose($file);

/*********************************************************************************************************/
// SUMMARIZE

$finishTime = time();
$totalTime = $finishTime - $startTime;
echo "\n\n" . $lineCounter - 1 . " rows processed in $totalTime seconds\n\n";


?>
