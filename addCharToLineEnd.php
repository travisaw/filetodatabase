<?php

$fileName = "words.txt";
$character = "|";

$arr = file($fileName);

foreach($arr as &$line){
    $line = preg_replace( "/\r|\n/", "", $line );
    $line = $line . $character;
}

unset($line); //just in case and a good thing to remember in other projects

file_put_contents($fileName, implode(PHP_EOL, $arr)); // write file with new line separating the array items.

?>